import {Component} from '@angular/core';

@Component({
    selector: 'app-cars',
    templateUrl: './cars.component.html',
    styleUrls: ['./cars.component.css']
})
export class CarsComponent {
    addCarStatus = '';

    constructor() {

    }

    inputText = 'Default text';

    onKetUp(event) {
        this.inputText = event.target.value;
    }

    addCar() {
        this.addCarStatus = 'Мащина добавлена';
    }
}
